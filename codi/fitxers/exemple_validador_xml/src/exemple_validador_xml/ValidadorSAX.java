package exemple_validador_xml;

import java.io.IOException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public class ValidadorSAX {
	public static void main(String args[]) {
		valida("alumnes.xml");
	}

	public static boolean valida(String xml) {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setValidating(true);
			factory.setNamespaceAware(true);
			SAXParser parser = factory.newSAXParser();
			XMLReader reader = parser.getXMLReader();
			reader.setErrorHandler(new ControladorErrades());
			reader.parse(new InputSource(xml));
			return true;
		} catch (ParserConfigurationException | IOException | SAXException e) {
			System.err.println(e);
			return false;
		}
	}
}
