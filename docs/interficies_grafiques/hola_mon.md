## *Hola món* en JavaFX

Per veure les bases de JavaFX podem començar amb analitzar una senzilla
aplicació d'*hola món*. Aquesta aplicació mostra un diàleg amb un botó. Quan
premem aquest botó apareix un diàleg emergent.

El codi és el següent:

[javafxHelloWorld](codi/utilitzacio_avancada_de_classes/javafx/javafxHelloWorld/src/javafxHelloWorld/HelloWorldFX.java)

Analitzem per línies:

- Línia 11: Una aplicació JavaFX sempre deriva de la classe *Application*.

- Línia 13: l'aplicació gràfica s'arrenca cridant el mètode estàtic
*Application.launch()*.

- Línia 17: el mètode *launch()* inicialitza el motor gràfic i crida al mètode
*start()* de la nostra *Application*. Noteu que *start()* ja no és estàtic.
Internament, *launch()* ha creat un objecte de la mateixa classe que l'ha
cridat, i després crida *start()* sobre aquest objecte.

    El mètode *start()* rep un objecte *Stage*. Aquest *primaryStage* és
    l'escenari principal on l'aplicació ha de construir els components gràfics
    que necessiti.

- Línies 18 i 19: creem un botó i li assignem un text.

- Línia 38: creem un panell on posar els nostres components. El panell
és de tipus *StackPane*, cosa que indica que es posaran els components un a
sobre de l'altre, des del fons cap al davant. En aquest cas ens és útil
perquè l'únic component (el botó) sortirà automàticament centrat.

- Línia 39: afegim el botó com un dels components de l'*StackPane*.

- Línia 41: creem l'escena principal. En aquesta aplicació només hi ha una
escena, però en aplicacions més complexes podríem tenir-ne vàries i assignar
a l'escenari quina escena volem mostrar en cada moment. Al constructor
d'*Scene* li passem l'element a mostrar (el panell) i la mida en píxels.

- Línia 43: posem un títol a la finestra.

- Línia 44: li diem a l'escenari que mostri l'escena que acabem de crear.

- Línia 45: fem visible la finestra.

- Línia 21: assignem l'acció que ha de fer el botó quan es premi.

- Línies 22-27: creem una finestra emergent amb el títol i text que volem. La
mostrem.

- Línies 29-36: així s'assigna l'acció al botó si no utilitzem una funció
lambda.
