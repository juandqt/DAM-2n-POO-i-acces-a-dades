## Diagrames de classes

### Exercicis

1. Dibuixa un diagrama de classes que representi un llibre, definit per la
sentència següent: "Un llibre es compon d'un nombre de parts, que a la
vegada es componen d'un cert nombre de capítols. Els capítols estan
compostos de seccions."

    Centra't només en les classes, relacions i multiplicitats.

2. Amplia el diagrama de classes de l'exercici anterior per incloure els
següents atributs.
 * Un llibre inclou un editor, una data de publicació i un ISBN.
 * Una part inclou un títol i un número.
 * Un capítol inclou un títol, un número i un resum.
 * Una secció inclou un títol i un número.

3. Considera el diagrama de classes de l'exercici anterior. Nota que les
classes *Part*, *Capítol* i *Secció* totes inclouen els atributs de *títol* i
*número*. Afegeix una classe abstracta i una relació de generalització per
trapassar aquests dos atributs a la classe abstracta.

4. Dibuixa un diagrama de classes que representi la relació entre pares i
fills. Tingues en compte que una persona pot tenir tant pares com fills.
Anota les associacions amb els noms dels rols i les multiplicitats.

5. Descriu la següent figura, identificant les classes i les seves
associacions:

    ![Diagrama de classes](docs/diagrames_uml/imatges/exercici_treballador_habilitats.png)

    Actualitza el diagrama pas a pas per incloure els següents detalls:
 * Quan un treballador té una habilitat, els anys d'experiència es
 mantenen a la relació.
 * Un treballador pot tenir un altre treballador com a encarregat, i un
 treballador que és un encarregat s'ha de fer càrrec de cinc o més
 treballadors. Donat un encarregat, es pot determinar qui està al seu
 càrrec, però donat un treballador, no es pot saber qui és el seu encarregat.
 * Una activitat no pot tenir més d'una activitat prèvia i qualsevol
 nombres d'activitats posteriors. Utilitzant això, podem mostrar com
 s'ordenen les activitats. Donada una activitat, es pot determinar quines
 són les següents activitats (si n'hi ha) però no la seva activitat prèvia
 (si en té una). Això és similar a com un equip pot estar fet de subequips
 en el sentit en què cal tenir un equip abans per poder tenir subequips.
 * Un treballador no està només associat a un conjunt d'habilitats,
 sinó que un treballador té habilitats. Específicament, un treballador ha
 de tenir tres o més habilitats, i qualsevol nombre de treballadors poden
 tenir la mateixa habilitat.
 * Un projecte no està només associat a un conjunt d'activitats, sinó
 que un projecte conté activitats. Específicament, un projecte ha de tenir
 una o més activitats, i una activitat ha de pertànyer a només un projecte.
 * Els projectes i les activitats són tipus específics de feina.

6. Descriu la següent figura identificant les classes i les seves associacions:

    ![Diagrama de classes](docs/diagrames_uml/imatges/exercici_treballador_habilitats2.png)

    Actualitza el diagrama pas a pas:
 * Un pla pertany a un únic projecte i implica a un o més treballadors
 i una o més activitats.
 * Un treballador pot tenir zero o més plans, i un pla ha de pertànyer
 a un únic treballador.
 * Cada pla conté un únic calendari. Donat un pla, es pot determinar el
 seu calendari, però donat un calendari, no es pot determina al pla
 al qual pertany.
 * Quan una activitat té un calendari, s'anomena una activitat programada,
 i la data d'inici, la data de finalització, i la durada es mantenen per
 a les activitats programades. Cada calendari pot tenir zero o més
 activitats, però una activitats s'ha d'associar a un únic calendari.
 * Una fita, un punt d'especial importància en el projecte, és un tipus
 especial d'activitat programada en què hi ha zero o més productes del
 treball que tenen un estatus específic. Una fita pot tenir qualsevol
 nombre de productes associats, i un producte pot estar relacionat amb
 qualsevol nombre de fites.

7. Volem informatitzar el funcionament d'una biblioteca. En concret, volem
un diagrama de classes per modelitzar els llibres de la biblioteca, els
seus socis i els préstecs que es produeixin.

    Podem tenir un o més exemplars de cadascun dels llibres disponibles
    a la biblioteca. Els exemplars constaran al catàleg de la biblioteca,
    i es poden donar d'alta o de baixa.

    Cada exemplar es pot trobar en un d'aquests estats: *disponible*, si es
    pot prestar, *prestat*, si en aquests moments el té algun soci en
    prèstec, o *no disponible*, si per exemple s'ha perdut o s'està
    reparant.

    Els exemplars es poden prestar i es poden retornar després. Només es
    poden prestar a socis i, evidentment, no es pot prestar el mateix
    exemplar dues vegades al mateix temps.

    Realitza el diagrama de classes d'aquest sistema, afegint els atributs
    que consideris bàsics.

8. Llegeix l'[enunciat del zoo](exercicis/zoo.md) i fes un diagrama de
classes que mostri com resoldries aquest problema. Siguis tan detallat
com sigui possible.
