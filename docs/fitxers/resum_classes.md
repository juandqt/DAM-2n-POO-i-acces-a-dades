


| Ús | Llegir text | Escriure text | Llegir binari | Escriure binari |
| -- |
| Classe abstracte base de les altres | Reader | Writer | InputStream | OutputStream |
| Bytes/caràcters individuals, o grups de bytes/caràcters. Classe principal per accedir a fitxers. | FileReader | FileWriter | FileInputStream | FileOutputStream |
| Dades simples (int, double, cadenes...) | - | - | DataInputStream | DataOutputStream |
| Buffers intermedis per reduir la quantitat d'accesssos a disc. Els de text permeten treballar línia a línia. | BufferedReader | BufferedWriter | BufferedInputStream | BufferedOutputStream
| Sortida amb format | - | - | - | PrintStream |
| Per objectes | - | - | ObjectInputStream | ObjectOutputStream |
| Transforma un flux d'un tipus en un altre | InputStreamReader (d'InputStream a Reader) | OutputStreamWriter (d'OutputStream a Writer) | - | - |
