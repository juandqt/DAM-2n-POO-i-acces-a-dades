import java.util.Comparator;

/**
 * Created by juan on 06/10/2015.
 */

/*
Creeu la classe Persona amb els atributs pes, edat i al�ada. Implementeu la interf�cie Comparable per fer que es compari l'al�ada.
Creeu un Comparator per permetre que dues persones es puguin comparar per l'edat, i un altre per tal que es puguin comparar pel pes.
*/

public class Persona /*implements Cloneable*/{
    private Double pes;
    private Integer edad;

    public Double getPes() {
        return pes;
    }
    public Integer getEdad() {

        return edad;
    }

    public Persona(Double pes, Integer edad) {
        this.pes = pes;
        this.edad = edad;
    }

    public static final Comparator<Persona> COMPARATOR_PES = (v1,v2)->  (v1.getPes().compareTo(v2.getPes())) ;

    public static final Comparator<Persona> COMPARATOR_EDAD = (v1, v2) -> v1.getEdad() - v2.getEdad();

    @Override
    public String toString() {
        return "Persona{" +
                "pes=" + pes +
                ", edad=" + edad +
                '}';
    }

    /*
    @Override
    protected Persona clone() throws CloneNotSupportedException {
        return (Persona)super.clone();
    }*/
}
