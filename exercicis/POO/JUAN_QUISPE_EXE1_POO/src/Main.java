import java.util.ArrayList;
import java.util.Collections;
/**
 * Created by juan on 06/10/2015.
 */

/*
Creeu la classe Persona amb els atributs pes, edat i al�ada. Implementeu la interf�cie Comparable per fer que es compari l'al�ada.
Creeu un Comparator per permetre que dues persones es puguin comparar per l'edat, i un altre per tal que es puguin comparar pel pes.
 */

public class Main {

    public static void main(String[] args) {

        ArrayList<Persona> personas = new ArrayList<>();
        personas.add(new Persona(23D,18));
        personas.add(new Persona(18D,16));
        personas.add(new Persona(3D,31));

        /*
        Persona persona = new Persona(3D,3);
        try{
            Persona clon = persona.clone();
            clon.getEdad();
            System.out.println(clon);

        }catch (Exception e){
            System.out.println("Molt be!");
        }
    */

        System.out.println("Comparador por edad:");
        Collections.sort(personas, Persona.COMPARATOR_EDAD);
        personas.forEach(n-> System.out.println(n.getEdad()));

        System.out.println("Comparador por peso");
        Collections.sort(personas, Persona.COMPARATOR_PES);
        personas.forEach(n-> System.out.println(n.getPes()));

    }
}
